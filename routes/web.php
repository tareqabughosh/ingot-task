<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['web', 'auth'])->group(function () {
    Route::resource('categories', 'CategoriesController');
    Route::resource('transactions', 'TransactionsController');
});

Route::prefix('admin')->middleware(['web', 'auth', 'admin'])->group(function () {
    Route::get('/', function(){
        return view('admin.index');
    })->name('dashboard');

    Route::get('users', 'Admin\UsersController@index')->name('users');
});
