## This is a vacancy task

To run this project do as follows:

- **[Download the project via git]
- **[Run composer update]
- **[Run npm install]
- **[Configure the env file]
- **[Run php artisan migrate]
- **[Serve]
