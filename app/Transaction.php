<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function scopeUser($query)
    {
        return $query->where('user_id', Auth()->user()->id);
    }  

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
