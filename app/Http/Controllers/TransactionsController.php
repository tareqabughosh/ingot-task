<?php

namespace App\Http\Controllers;

use App\Category;
use App\Transaction;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::with('category')->User()->paginate(10);

        return view('transactions.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Cache::remember('categories', 600, function () {
            return Category::User()->select('id', 'name')->get();
        });

        return view('transactions.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric|between:-999999,999999',
            'category_id' => 'required',
            'note' => 'required',
        ]);

        $cat_check = Category::where(['id' => $request->category_id, 'user_id' => Auth()->user()->id])->first();
        if(!$cat_check){
            return back()->with('error', 'Please select a valida category');
        }
        $tran = DB::transaction(function() use($request){
            try{
                $tran = new Transaction();
                $tran->amount = $request->amount;
                $tran->category_id = $request->category_id;
                $tran->note = $request->note;
                $tran->user_id = Auth()->user()->id;
                $tran->wallet_balance = Auth()->user()->wallet;
                $tran->save();
        
                Auth()->user()->wallet += $request->amount;
                Auth()->user()->save();
    
                return true;
            } catch(\Exception $e){
                return false;
            }
        });
        if($tran){
            return back()->with('success', 'Added successfully!');
        }
        return back()->with('error', 'Something went wrong!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
