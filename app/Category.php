<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Category extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'name'];

    public function scopeUser($query)
    {
        return $query->where('user_id', Auth()->user()->id);
    }
}
