$(document).ready(function() {
    /** Sliders **/
    $('.slider').show();

    /** hide tasts messages **/
    setTimeout(function() {
        $('.toasts-messages').hide('slow');
    }, 5000);

    $(".lazy").slick({
        lazyLoad: 'ondemand',
        infinite: true,
        dots: true,
        arrows: false
    });

    $('.advisor-slider').slick({
        lazyLoad: 'ondemand',
        infinite: true,
        dots: false,
        arrows: true,
        infinite: true,
        slidesToShow: 5.2,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 1550,
                settings: {
                  slidesToShow: 4,
                }
            },
            {
                breakpoint: 1450,
                settings: {
                  slidesToShow: 3.7,
                }
            },
            {
                breakpoint: 1200,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                }
            },
            {
              breakpoint: 786,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              }
            },
            {
                breakpoint: 586,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                }
            },
        ],
    });
    /** End sliders **/
    /** Open header search **/
    $('.header-search-icon').click(function() {
        $('.form-search').toggle('slide');
        $('.default-header .header').toggleClass('mobile-search');
        setTimeout(function() {
            if($('.form-search').is(":visible")) {
                $('.header-serch-input').focus();
            }
        }, 500)
    });

    /** Open header profile menu **/
    $('.header-dots').click(function(event) {
        event.stopPropagation();
        $('.profile-menu').toggle('slide');
        $('.profile-menu-mobile').hide('slow');
    });

    /** Open header profile menu mobile **/
    $('.header-mobile-icon').click(function(event) {
        event.stopPropagation();
        $('.profile-menu').hide('slow');
        $('.profile-menu-mobile').toggle('slide');
    });

    /** Close menus when click on document **/
    $(document).click(function() {
        $('.profile-menu').hide('slow');
        $('.profile-menu-mobile').hide('slow');
    });

    /** make header sticky on scroll down **/
    makeHeaderSticky();
    $(document).on("scroll", function(e) {
        makeHeaderSticky();
    });

    /** filter sotr change **/
    $('.sort-filter').on('change', function() {
        $('#sortForm').submit();
    });

    /** change profile picture event **/
    $('.edit-image-icon').on('click', function() {
        event.stopPropagation();
        $(".profile-img-input").trigger('click');
    });

    /** change profile picture **/
    $(".profile-img-input").change(function() {
        readURL(this);
    });

    /** upload file click **/
    $(".aupload-field.default-field").click(function() {
        $(this).next('input.file-field').trigger('click');
    });

    /** select single file **/
    $(".file-field.single").change(function() {
        showSelectedFile(this);
    });

    /** select multible file **/
    $(".file-field.multible").change(function() {
        showMultibleFiles(this);
    });

    /** select multible file to share with advisor **/
    $(".file-field.share").change(function() {
        $(".file-field.share").files = this.files;
        $('.shareFolesForm').submit();
    });

    /** delete file **/
    $('.multible-files').on('click', '.close-file-icon', function(event) {
        event.stopPropagation();
        removeFile(this);
    });

    /** delete shared file **/
    $('.multible-files').on('click', '.remove-shared-file', function(event) {
        event.stopPropagation();
        removeSharedFile(this);
    });

    $('.multible-files').on('click', '.file-block', function(event) {
        event.stopPropagation();
    });

    /** change country **/
    $('#country').change(function() {
        var sekectedOption = $(this).find('option:selected');
        var countryCode = sekectedOption.attr('data-country-code');
        var countryCurrency = sekectedOption.attr('data-country-currency');
        if(countryCode) {
            $('#countryCodeSpan').text(countryCode);
            $('#countryCodeInput').val(countryCode);
        }
        // if(countryCurrency) {
        //     $('#countryCurrencySpan').text(countryCurrency);
        //     $('#countryCurrencyInput').val(countryCurrency);
        // }
    });

    /** show next step signup **/
    $('.show-next-signup').click(function() {
        $('.signup-first-step').css('display','none');
        $('.signup-second-step').css('display','flex');
        window.scrollTo(0, 10);
    });

    /** show previous step signup **/
    $('.show-prev-btn').click(function() {
        $('.signup-first-step').css('display','flex');
        $('.signup-second-step').css('display','none');
        window.scrollTo(0, 10);
    });

    /** remove specialty row **/
    $('.specialty-group').on('click', '.remove-specialty-row', function() {
        $(this).parent().remove();
        $('.add-specialty-btn').show();
    });

    /** show password event **/
    $('.show-password-icon').on('click', function() {
        if(!$(this).hasClass('show')) {
            $(this).prev().prop('type', 'text');
            $(this).addClass('show');
        }
        else {
            $(this).prev().prop('type', 'password');
            $(this).removeClass('show');
        }
    });

    /** remove disabled on all days **/
    $('.remove-disabled').click(function() {
        $('.days-list input').removeAttr('disabled');
        $('.selected-times').hide();
        $('.add-availabel-times').show();
    });

    /** prevent to select previous date on availability time page **/
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var today = tomorrow.toISOString().split('T')[0];
    $('.to-date-availability').attr('min', today);

    /** create today date field **/
    getCurrentDate();

    /** select time from - to reservation page **/
    $('.times-list').on('click', '.times-block-btn', function() {
        if($(this).hasClass('selected')) {
            $('.ammount').hide();
            $('.after-coupon-price').hide();
            $('.copoun-section').hide();
            $('.book-now').attr('disabled', true);
            $(this).removeClass('selected');
            $('.selectedTimeFrom').val(null);
            $('.selectedTimeTo').val(null);
        }
        else {
            $('.ammount').show();
            if($('.new-price').text() && $('.new-price').text() != 0) {
                $('.after-coupon-price').show();
            }
            $('.copoun-section').css('display','flex');
            $('.book-now').attr('disabled', false);
            $('input.selectedTimes').val();
            $('.times-block-btn').removeClass('selected');
            $(this).addClass('selected');
            $('.selectedTimeFrom').val($(this).attr('data-time-from'));
            $('.selectedTimeTo').val($(this).attr('data-time-To'));
        }
    });

    /** select day from calendar my calendar page **/
    $('.select-time-calendar.my-calendar').on('click', '.fc-day-top:not(.fc-past)', function() {
        var date = $(this).attr('data-date');
        $('.date').val(date);
        if(date < new Date()) return;

        $('.fc-day-top').removeClass('selected');
        $(this).addClass('selected');
        $.ajax({
            url: "/booking/calendar/"+date,
            method: "GET",
        })
        .done(function(data) {
            $('.meetings-list').html('');
            $('.selected-date').hide();
            if(data.data.length < 1) {
                $('.no-data').show();
                return;
            }

            $('.no-data').hide();
            data.data.map(itm => {
                $('.meetings-list').append(`
                    <div class="col-12 meeting-item">
                        <a href="`+itm.booking_id+`" class="d-flex align-items-center">
                            <div class="header-profile-img d-block" style="background-image: url('`+itm.avatar+`"></div>
                            <div class="meeting-item-content">
                                <h3 class="availability-advisor-name w-auto d-inline-block">`+itm.name+`</h3>
                                <span class="sub-data">- `+itm.topic+`</span>
                                <span class="sub-data d-block w-100 c-purple">`+itm.date+`</span>
                                <span class="sub-data d-block w-100">`+itm.from+` - `+itm.to+`</span>
                            </div>
                        </a>
                    </div>
                `);
            });
        })
        .fail(function(data) {
            alert('Request Error');
            return;
        });
    });

    /** select day from calendar reservation page **/
    $('.select-time-calendar.reserve-page').on('click', '.fc-day-top:not(.fc-past,.fc-today)', function() {
        var date = $(this).attr('data-date');
        $('.date').val(date);
        if(date < new Date()) return;

        $('.fc-day-top').removeClass('selected');
        $(this).addClass('selected');
        $.ajax({
            url: "/advisor/get-availabiles/"+date+"/"+$('.advisorId').val(),
            method: "GET",
        })
        .done(function(data) {
            if(!data) return;

            $('.availability-advisor-error-date').hide();
            $('.availability-advisor-topic').show();
            $('.ammount').hide();
            $('.after-coupon-price').hide();
            $('.copoun-section').hide();
            $('.book-now').attr('disabled', true);
            $('.times-list').html('');
            data.data.map(itm => {
                $('.times-list').append(`
                    <div class="col-12 mb-3">
                        <button type="button" data-time-from="`+itm.from+`" data-time-to="`+itm.to+`" class="btn times-block-btn">`+itm.fromVisual+` - `+itm.toVisual+`</button>
                    </div>
                `);
            });
            $('.selected-date').text(data.date);
            $('.price').text(data.price);
            $('.currency').text(data.currency);
        })
        .fail(function(data) {
            $('.availability-advisor-error-date').show();
            $('.availability-advisor-topic').hide();
            $('.ammount').hide();
            $('.after-coupon-price').hide();
            $('.copoun-section').hide();
            $('.book-now').attr('disabled', true);
            $('.times-list').html('');
            $('.selected-date').text(date);
            return;
        });
    });

    /** select day from calendar reservation page **/
    $('.reschedule-input').change(function() {
        var date = $(this).val();
        $('.date').val(date);
        if(new Date(date) < new Date()) {
            $('.times-list').html('');
            $('.book-now').attr('disabled', true);
            $('.rescheduale-date').show();
            return;
        }

        $.ajax({
            url: "/advisor/get-availabiles/"+date+"/"+$('.advisorId').val(),
            method: "GET",
        })
        .done(function(data) {
            if(!data) return;

            $('.book-now').attr('disabled', true);
            $('.times-list').html('');
            $('.rescheduale-date').hide();
            data.data.map(itm => {
                $('.times-list').append(`
                    <div class="col-12 mb-3">
                        <button type="button" data-time-from="`+itm.from+`" data-time-to="`+itm.to+`" class="btn times-block-btn">`+itm.fromVisual+` - `+itm.toVisual+`</button>
                    </div>
                `);
            });
        })
        .fail(function(data) {
            $('.book-now').attr('disabled', true);
            $('.times-list').html('');
            $('.rescheduale-date').show();
            return;
        });
    });

    /** verified copoun button event **/
    $('.verified-copoun').click(function() {
        var copoun = $('.copoun-field input').val();
        var subTopicId = $('#subTopics').val();
        $.ajaxSetup({ cache: false,headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });

        if(!subTopicId) {
            $('.copoun-field-error').show();
            $('.coupon-error').hide();
            return;
        }
        else {
            $('.copoun-field-error').hide();
        }
        if(!copoun) {
            $('.copoun-field input').addClass('is-invalid');
        }
        else {
            $('.copoun-field .is-invalid').removeClass('is-invalid');
        }

        $.ajax({
            url: "/coupon/validate",
            method: "POST",
            data: {advisor_id: $('.advisorId').val(), coupon: copoun, subTopicId: subTopicId}
        })
        .done(function(data) {
            $('.real-price').css('text-decoration','line-through');
            $('.new-price').text(data.price);
            $('.after-coupon-price').show();
            $('.coupon-error').hide();
            $('.copoun-field .is-invalid').removeClass('is-invalid');
            $('.copoun-field input').addClass('is-valid');
            if(data.price === 0) {
                $('.selectPayment select').prop('required', false);
                $('.selectPayment').hide();
            }
            else {
                $('.selectPayment select').prop('required', true);
                $('.selectPayment').show();
            }
            // $('.copoun-field input').attr('disabled', true);
        })
        .fail(function(data) {
            $('.real-price').css('text-decoration','initial');
            $('.new-currency').text('0');
            $('.after-coupon-price').hide();
            $('.new-currency').hide();
            $('.coupon-error').show();
            $('.copoun-field input').addClass('is-invalid');
            $('.selectPayment select').prop('required', true);
            $('.selectPayment').show();
        })
    });

    /** check phone number validation **/
    $('.phone-number').keyup(function(event) {
        if(!Number(event.originalEvent.key) && event.originalEvent.key != '0'){
            $(this).val($(this).val().replace(event.originalEvent.key, ''));
        }
    });

    /** instant booking book now button event **/
    $('.book-now-instant').click(function() {
        if($('#type').val()){
            $('.book-now-field-error').hide();
        }
        else {
            $('.book-now-field-error').show();
            return;
        }
        $.ajaxSetup({ cache: false,headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        var advisorId = $('.advisorId').val();
        var subTopic = $('.subTopic').val();
        var type = $('#type').val();
        $.ajax({
            url: "/instant-booking",
            method: "POST",
            data: {advisor_id: advisorId, subtopic: subTopic, type}
        })
        .done(function(data) {
            $('.instant-text').text(data.status);
            // $('.cancel-instant').remove();
            $('.book-now-instant').hide();
        })
        .fail(function(data) {
            // alert('Error Happend');
        })
    });

    $('.switch').click(function(event) {
        event.stopPropagation();
    });

    /** instant toggle on header menu **/
    $('.instant-toggle').change(function() {
        var instant = false;
        if($(this).prop("checked") === true) {
            instant = true;
        }

        $.ajaxSetup({ cache: false,headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });

        $.ajax({
            url: "/advisor/instant-book",
            method: "POST",
            data: {instant: instant}
        })
        .done(function(data) {
            console.log(data);
        })
        .fail(function(data) {
            console.log(data);
        });
    });
});

$(window).on("load", function() {
    $('.lds-ring').css('display','none');
});
/*** make header sticky on scroll down***/
function makeHeaderSticky() {
    if (window.pageYOffset > 30) {
        $('.default-header').addClass("sticky");
    } else {
        $('.default-header').removeClass("sticky");
    }
}

/*** change profile image ***/
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.profile-img-holder').css("background-image", "url(" + e.target.result + ")");
        }
        reader.readAsDataURL(input.files[0]);
    }
}

/*** show selected file name ***/
function showSelectedFile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(input).prev().find('.upload-hint').text(input.value);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

/** show multible selected files **/
var certifications = [];
function showMultibleFiles(input) {
    if (input.files.length > 0 && input.files) {
        for(i = 0; i < input.files.length; i++) {
            if(!checkIfObjectExist(input.files[i], certifications)) {
                certifications.push(input.files[i]);
                var fileName = input.files[i].name;
                var type;
                if(fileName.includes('.docs') || fileName.includes('.doc')) {
                    type = 'word';
                }
                else if (fileName.includes('.pdf')) {
                    type = 'pdf';
                }
                else if (fileName.includes('.txt')) {
                    type = 'txt';
                }
                else if (fileName.includes('.jpg') || fileName.includes('.jpeg') || fileName.includes('.JPG') || fileName.includes('.JPEG') || fileName.includes('.png') || fileName.includes('.PNG')) {
                    type = 'img';
                }
                else {
                    type = 'default-file';
                }
                $('.selected-files-list').prepend(
                    `<div class="file-block `+type+`">
                        <label class="field-label">`+input.files[i].name+`</label>
                        <button type="button" class="close-file-icon" href="javascript:;">
                            <i class="icon-close-5"></i>
                        </button>
                    </div>`
                );
            }
        }
    }
}

/** remove file **/
function removeFile(item) {
    var removedItem = $(item).prev().text();
    $(item).parent().remove();
    newCertifications = [];
    for(i = 0; i < certifications.length; i++) {
        if(certifications[i].name !== removedItem) {
            newCertifications.push(certifications[i]);
        }
    }
    certifications = newCertifications;
    if(certifications.length == 0) {
        $('#certifications').val(null);
    }
    $('#certifications').files = certifications;
}

/** remove shared file **/
function removeSharedFile(item) {
    var removedItem = $(item).prev().text();
    $(item).parent().remove();
    newSharedFiles = [];
    for(i = 0; i < sharedFiles.length; i++) {
        if(sharedFiles[i].name !== removedItem) {
            newSharedFiles.push(sharedFiles[i]);
        }
    }
    sharedFiles = newSharedFiles;
    if(sharedFiles.length == 0) {
        $('#sharedFiles').val(null);
    }
    $('#sharedFiles').files = sharedFiles;
    $.ajaxSetup({ cache: false,headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }});
    var fileId = $(item).attr('data-id');
    var booking_id = $('.booking-id').val();
    $.ajax({
        url: "/session/file/delete",
        method: "POST",
        data: {file_id: fileId, booking_id: booking_id}
    })
    .done(function(data) {
        console.log(data);
    })
    .fail(function(data) {
        alert('Error Happend');
    })
}

function checkIfObjectExist(obj, arrObj) {
    if(arrObj.findIndex(item => item.name === obj.name) !== -1) {
        return true;
    }
    return false;
}

/** create today date field **/
function getCurrentDate() {
    var d = new Date(),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();
    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    $('.today_label').text([year, month, day].join('-'));
}

/** download file **/
function downloadFile(filePath) {
    var link = document.createElement("a");
    link.setAttribute("download", 'aa');
    link.href = filePath;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
}

/** download uploaded file **/
function downloaduploadedFile(filePath, e) {
    e.stopPropagation();
    var link = document.createElement("a");
    link.setAttribute("download", 'aa');
    link.href = filePath;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
}

/** replace all string **/
function replaceAll(string, search, replace) {
    return string.split(search).join(replace);
}

function getSubSpecialities(element) {
    var mainSpecialities = [];
    var specialty = $('.specialty');
    for (let index = 0; index < specialty.length; index++) {
        if($(specialty[index]).val()) {
            mainSpecialities.push(Number($(specialty[index]).val()));
        }
    }

    mainSpecialities.splice(mainSpecialities.indexOf(Number($(element).val())), 1);
    if(mainSpecialities.indexOf(Number($(element).val())) !== -1) {
        $('.speciality-error').show();
        $(element).val(null);
        var nextSub = $(element).parent().parent().parent().next().find('select.subspecialty');
        $(nextSub).html(null);
        $(element).selectpicker('refresh');
        $(nextSub).selectpicker('refresh');
        return;
    }
    else {
        $('.speciality-error').hide();
    }

    $.ajax({
        url: "/topic/get-subtopics/"+$(element).val(),
        method: "GET",
    })
    .done(function(data) {
        if(!data) return;
        var nextSub = $(element).parent().parent().parent().next().find('select.subspecialty');
        nextSub.val(null).html(null);
        data.topics.map(itm => {
            $(nextSub).append(`
                <option value="`+itm.id+`">`+itm.name+`</option>
            `);
        });
        $(nextSub).selectpicker('refresh');
    })
    .fail(function(data) {
        alert('Request Error');
        return;
    });
}

/** Mark Notification as read **/
function markAsRead(id) {
    var noti_id = id;
    $.ajax({
        url: "/markasread",
        method: "GET",
        data: {id : noti_id},
    })
    .done(function(data) {
    })
    .fail(function(data) {
    });
}

/** show profile image on popup **/
function showProfileImage(url)
{
    if(url) {
        $('.profile-image-modal').attr('src', url);
        $('.view-profile-image-modal').modal('show');
    }
}
