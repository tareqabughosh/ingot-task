@include('layouts/layout/header')

<head>
    @yield('head')
    <style>
        .dropdown-menu.dropdown-menu-fit {
            border-radius: 10px;
            overflow: hidden;
        }

        .kt-header__topbar-item--user.show .fa-bell {
            color: #494b74;
        }

        .kt-aside__brand-logo img {
            width: 60px;
        }

        .bell-icon button {
            background-color: transparent;
            border: 0;
            color: #aaa;
            font-size: 30px;
            position: relative;
        }

        .bell-icon button.new::after {
            content: "";
            position: absolute;
            top: -2px;
            right: 1px;
            width: 15px;
            height: 15px;
            background-color: red;
            border-radius: 50%;
            box-shadow: 0px 0px 5px 2px #ff000085;
        }

        .notifications-list {
            padding: 0;
            margin: 0;
            list-style: none;
            width: 100%;
            overflow: auto;
            max-height: 300px;
        }

        .notifications-list li {
            border-bottom: 1px solid #efefef;
        }

        .notifications-list li a {
            width: 100%;
            padding: 13px 10px;
        }

        .notifications-list .notification-title {
            font-size: 14px;
            font-weight: 500;
            color: #555;
            padding-right: 10px;
        }

        .notifications-list .notification-time {
            color: #2dc0d5;
            font-size: 10px;
            width: 110px;
            text-align: right;
        }

        .notifications-list li a:hover {
            background-color: #efefef;
        }

        .view-all-notifications {
            font-weight: 600;
        }

        .view-all-notifications:hover {
            color: #494b74;
        }

        button.kt-input-icon__icon.kt-input-icon__icon--right {
            background: transparent;
            border: 0;
        }

        .bootstrap-select .dropdown-menu.show {
            max-height: 200px!important;
        }
    </style>
</head>

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
    @include('layouts/layout/sidebar')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
        <!-- begin:: Header -->
        <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
            <div class="col-md-4">
                <a class="back-button btn" id="goback" role="button"><i class="fa fa-chevron-left" aria-hidden="true"></i>Back</a>
            </div>
            <div class="kt-header__topbar">
                <div class="kt-header__topbar-item dropdown">
                    <div class="kt-header__topbar-wrapper row" data-offset="30px,0px" aria-expanded="true">
                    </div>
                </div>
                <!--end: Quick Actions -->
                <!--begin: My Cart -->
                <div class="kt-header__topbar-item kt-header__topbar-item--user">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                        <div class="kt-header__topbar-user">
                            <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                            <span class="kt-header__topbar-username kt-hidden-mobile">{{Auth()->check() ? auth()->user()->name : ''}}</span>
                            <img class="kt-hidden" alt="Pic" src="/assets/media/users/300_25.jpg" />
                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{ Auth()->check() ? Auth()->user()->name[0] : ''}}</span>
                        </div>
                    </div>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                        <!--end: Head -->
                        <!--begin: Navigation -->
                        <div class="kt-notification">
                            <div class="kt-notification__custom kt-space-between">
                                <form action="{{route('logout')}}" method="POST" style="margin: auto;">
                                @csrf
                                    <button type="submit" class="btn btn-label btn-label-brand btn-sm btn-bold">
                                        <i class="fa fa-sign-out-alt"></i> Log out
                                    </button>
                                </form>
                            </div>
                        </div>
                        <!--end: Navigation -->
                    </div>
                </div>
                <div class="kt-header__topbar-item kt-header__topbar-item--user mx-2">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                        <div class="bell-icon kt-header__topbar-user">
                            <button class="fa fa-bell new"></button>
                        </div>
                    </div>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                        <!--end: Head -->
                    </div>
                </div>
                <!--end: User Bar -->
            </div>
            <!-- end:: Header Topbar -->
        </div>
        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
            <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-subheader__main">
                        @yield('body-header')
                    </div>
                </div>
            </div>
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="row">
                        @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            <div class="alert-icon"><i class="flaticon-success"></i></div>
                            <div class="alert-text">{{session("success")}}</div>
                        </div>
                        @endif
                        @if(session('error'))
                        <div class="alert alert-danger" role="alert">
                            <div class="alert-icon"><i class="flaticon-danger"></i></div>
                            <div class="alert-text">{{session("error")}}</div>
                        </div>
                        @endif
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    @yield('body')
                </div>
            </div>
        </div>
        <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-footer__copyright">
                    2020&nbsp;&copy;&nbsp;<a target="_blank" class="kt-link">Atomkit All Rights Reserved</a>
                </div>
            </div>
        </div>
    </div>
    @include('layouts/layout/scripts')
    @yield('scripts')
</body>
