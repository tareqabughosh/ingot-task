@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="text-right">
                <a href="{{route('categories.create')}}" class="btn btn-primary">
                    Add a category
                </a>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
              </tr>
            </thead>
            <tbody>
                @foreach($categories as $cat)
              <tr>
                <th scope="row">{{ $cat->id }}</th>
                <td>{{ $cat->name }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $categories->links() }}
    </div>
</div>
@endsection
