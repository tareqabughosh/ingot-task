@extends('layouts.layout.layout')

@section('head')
<title>Users</title>
<style>
	.search-box {
		position: absolute;
		left: 0;
		top: 50px;
		left: 15px;
		z-index: 250;
		border-radius: 2px;
		border: 1px solid #aaa;
		padding: 5px;
		display: none;
	}
	.custom-search-select.show + .search-box {
		display: block;
	}
	.custom-search-select .dropdown-menu {
		padding-top: 50px;
	}
	.custom-search-select .inner {
		max-height: 140px!important;
	}
</style>
@endsection

@section('body-header')

@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <form action="#" method="POST">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                    <table class="kt-datatable__table" style="display: block;">
                        <thead class="kt-datatable__head">
                            <tr class="kt-datatable__row" style="left: 0px;">
                                <th class="kt-datatable__cell"><span style="width: 100px;">ID</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Name</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Email</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Birthdate</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Phone</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Wallet</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Total expenses</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Total Income</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Date created</span></th>
                            </tr>
                        </thead>
                        <tbody style="" class="kt-datatable__body">
                            @foreach($users as $key=>$user)
                            <tr data-row="{{$user->id}}" class="kt-datatable__row">
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$user->id}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$user->name}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$user->email}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$user->birthdate}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$user->phone}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$user->wallet}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$user->expenses_count}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$user->income_count}}</span>
                                </td>
                                <td data-field="Type" data-autohide-disabled="false" class="kt-datatable__cell">
                                    <span style="width: 100px;">{{$user->created_at->format('d/m/Y')}}</span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="kt-pagination kt-pagination--brand">
                {{$users->links()}}
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
@endsection
