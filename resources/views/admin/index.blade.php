@extends('layouts.layout.layout')

@section('head')
<title>Dashboard</title>
<style>
	.search-box {
		position: absolute;
		left: 0;
		top: 50px;
		left: 15px;
		z-index: 250;
		border-radius: 2px;
		border: 1px solid #aaa;
		padding: 5px;
		display: none;
	}
	.custom-search-select.show + .search-box {
		display: block;
	}
	.custom-search-select .dropdown-menu {
		padding-top: 50px;
	}
	.custom-search-select .inner {
		max-height: 140px!important;
	}
</style>
@endsection

@section('body-header')

@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <!-- begin:: Content Head -->
    <div class="kt-subheader  kt-grid__item row" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Dashboard</h3>
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                    <form action="" method="GET" id="submittable">
						<input type="text" class="search-box">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection
