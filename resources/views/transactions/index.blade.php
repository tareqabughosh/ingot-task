@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="text-right">
                <a href="{{route('transactions.create')}}" class="btn btn-primary">
                    Add a transaction
                </a>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Category</th>
                <th scope="col">Amount</th>
                <th scope="col">Note</th>
                <th scope="col">Wallet balance before</th>
              </tr>
            </thead>
            <tbody>
              @php
                  $total_income = 0;
                  $total_expenses = 0;
              @endphp
                @foreach($transactions as $trans)
              <tr>
                <th scope="row">{{ $trans->id }}</th>
                <td>{{ $trans->category->name }}</td>
                <td>{{ $trans->amount }}</td>
                <td>{{ $trans->note }}</td>
                <td>{{ $trans->wallet_balance }}</td>
              </tr>
              @php
              if($trans->amount > 0)
                  $total_income += $trans->amount;
              else
                  $total_expenses += $trans->amount;
              @endphp
              @endforeach
            </tbody>
            <tfoot>
              <tr>
              <td></td>
              <td></td>
                <td>Total Income: {{$total_income}}</td>
                <td>Total expenses: {{$total_expenses}}</td>
                <td>Wallet: {{Auth()->user()->wallet}}</td>
              </tr>
            </tfoot>
          </table>
          {{ $transactions->links() }}
    </div>
</div>
@endsection
